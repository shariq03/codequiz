﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CodeQuiz.Core.Contracts
{
    public interface IAnagramDetectionService
    {
        int DetectAnagram(string parent, string query);
    }
}
