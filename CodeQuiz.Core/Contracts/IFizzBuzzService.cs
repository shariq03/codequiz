﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CodeQuiz.Core.Contracts
{
    public interface IFizzBuzzService
    {
        Dictionary<int, string> GenerateNumbers(int limit);
    }
}
