﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CodeQuiz.Core.Contracts
{
    public interface IBalancedBracketsService
    {
        /// <summary>
        /// Generates the brackets.
        /// </summary>
        /// <returns>true if balanced, false if not balanced</returns>
        bool GenerateBrackets(string input);
    }
}
