﻿using CodeQuiz.Core.Contracts;
using System;
using System.Collections.Generic;
using System.Linq;

namespace CodeQuiz.Core.Implementation
{
    public class BalancedBracketsService : IBalancedBracketsService
    {
        private Dictionary<char, char> _allowedBrackets;
        /// <summary>
        /// Initializes a new instance of the <see cref="BalancedBracketsService"/> class.
        /// </summary>
        public BalancedBracketsService()
        {
            _allowedBrackets = AllowedBrackets();
        }

        /// <summary>
        /// Generates the brackets.
        /// </summary>
        /// <param name="input"></param>
        /// <returns>
        /// true if balanced, false if not balanced
        /// </returns>
        public bool GenerateBrackets(string input)
        {
            Stack<string> stack = new Stack<string>();
            bool isBalanced = true;
            //check for empty string, empty string will be a balanced string
            if (string.IsNullOrEmpty(input))
                return isBalanced;
            else
            {
                //convert into to char array
                char[] charInput = input.ToCharArray();
                foreach (var c in charInput)
                {
                    //check if provided char exist in _allowedBrackets dictionary as key
                    if (_allowedBrackets.ContainsKey(c))
                    {
                        //push into stack
                        stack.Push(c.ToString());
                    }
                    //check if provided char exist in _allowedBrackets dictionary as value
                    else if (_allowedBrackets.ContainsValue(c))
                    {
                        //if there is any key check and make it as balanced
                        isBalanced = stack.Any();
                        if (isBalanced)
                        {
                            // hit ending char grab previous starting char
                            var startingChar = stack.Pop();
                            // check it is in the dictionary
                            isBalanced = _allowedBrackets.Contains(new KeyValuePair<char, char>(Convert.ToChar(startingChar), c));
                        }
                        // if not isBalanced exit loop no need to continue
                        if (!isBalanced)
                        {
                            break;
                        }
                    }
                }
            }
            return isBalanced;
        }

        /// <summary>
        /// Alloweds the brackets.
        /// </summary>
        /// <returns></returns>
        private Dictionary<char, char> AllowedBrackets()
        {
            Dictionary<char, char> allowedBrackets = new Dictionary<char, char>();
            allowedBrackets.Add('(', ')');
            allowedBrackets.Add('[', ']');
            allowedBrackets.Add('{', '}');
            return allowedBrackets;
        }
    }
}
