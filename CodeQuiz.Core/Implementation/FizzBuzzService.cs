﻿using CodeQuiz.Core.Contracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CodeQuiz.Core.Implementation
{
    public class FizzBuzzService : IFizzBuzzService
    {
        public Dictionary<int, string> GenerateNumbers(int limit)
        {
            Dictionary<int, string> numbers = new Dictionary<int, string>();

            for (int n = 1; n <= limit; n++)
            {
                //check for if it is multiple of 3 and 5
                if (n % 3 == 0 && n % 5 == 0)
                {
                    numbers.Add(n, "FizzBuzz");
                }
                //check for if it is multiple of 3
                else if (n % 3 == 0)
                {
                    numbers.Add(n, "Fizz");
                }
                //check for if it is multiple of 5
                else if (n % 5 == 0)
                {
                    numbers.Add(n, "Buzz");
                }
                //for everything else
                else
                {
                    numbers.Add(n, "");
                }
            }
            return numbers;
        }
    }
}
