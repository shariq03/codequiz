﻿using CodeQuiz.Core.Contracts;
using System;

namespace CodeQuiz.Core.Implementation
{
    public class AnagramDetectionService : IAnagramDetectionService
    {
        public int DetectAnagram(string parent, string query)
        {
            //convert string to lower for better comparision
            parent = parent.ToLower();
            query = query.ToLower();

            //Convert the string to Character Array
            char[] charParent = parent.ToCharArray();
            char[] charQuery = query.ToCharArray();
            
            int anagramCount = 0;
            //check the the length of parent and query string if it is 0 it should return 0 Anagram detection
            if (query.Length == 0 || parent.Length == 0)
            {
                return anagramCount;
            }
            else
            {
                //Sort the character Array
                Array.Sort(charParent);
                Array.Sort(charQuery);

                //Loop through all elements in both character Array
                for (int i = 0; i < charQuery.Length; i++)
                {
                    //check all characters are equal in sorted char array
                    if (charParent[i] == charQuery[i])
                    {
                        //Index reaches string length
                        //all characters are equal
                        if (i == charParent.Length - 1)
                        {
                            anagramCount++;
                        }
                        //continue loop through all characters in the char array
                        continue;
                    }
                    //when any character in char array not equals
                    else
                    {
                        anagramCount = 0;
                        break;
                    }
                }
            }
            return anagramCount;
        }
    }
}
