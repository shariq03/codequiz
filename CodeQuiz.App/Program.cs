﻿using CodeQuiz.Core.Contracts;
using CodeQuiz.Core.Implementation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CodeQuiz.App
{
    class Program
    {
        static void Main(string[] args)
        {
            //FizzBuzz Problem
            Console.WriteLine("Input number limit:");

            IFizzBuzzService serv = new FizzBuzzService();
            Dictionary<int, string> numbers = serv.GenerateNumbers(100);
            foreach (var n in numbers)
            {
                Console.WriteLine(string.Format("{0}: {1}", n.Key, n.Value));
            }
            Console.ReadLine();
        }
    }
}
