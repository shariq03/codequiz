﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using CodeQuiz.Core.Contracts;
using CodeQuiz.Core.Implementation;

namespace CodeQuiz.Tests
{
    [TestClass]
    public class FizzBuzzTests
    {
        IFizzBuzzService _service;
        [TestInitialize]
        public void Init()
        {
            _service = new FizzBuzzService();
        }

        [TestMethod]
        public void FizzBuzz_Return_Third_ShouldReturnFizz()
        {
            var num = _service.GenerateNumbers(5);
            Assert.AreEqual("Fizz", num[3]);
        }

        [TestMethod]
        public void FizzBuzz_Return_Five_ShouldReturnBuzz()
        {
            var num = _service.GenerateNumbers(5);
            Assert.AreEqual("Buzz", num[5]);
        }

        [TestMethod]
        public void FizzBuzz_Return_Fifteen_ShouldReturnFizzBuzz()
        {
            var num = _service.GenerateNumbers(20);
            Assert.AreEqual("FizzBuzz", num[15]);
        }
    }
}
