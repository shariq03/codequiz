﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using CodeQuiz.Core.Contracts;
using CodeQuiz.Core.Implementation;

namespace CodeQuiz.Tests
{
    [TestClass]
    public class BalancedBracketsTests
    {
        [TestMethod]
        public void BalancedBrackets_Returns_BalancedBrackets()
        {
            IBalancedBracketsService serv = new BalancedBracketsService();
            bool result = serv.GenerateBrackets("()()");
            Assert.AreEqual(true, result);
        }

        [TestMethod]
        public void BalancedBrackets_Returns_NotBalancedBrackets()
        {
            IBalancedBracketsService serv = new BalancedBracketsService();
            bool result = serv.GenerateBrackets("[(){(}");
            Assert.AreEqual(false, result);
        }
    }
}
