﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using CodeQuiz.Core.Contracts;
using CodeQuiz.Core.Implementation;

namespace CodeQuiz.Tests
{
    [TestClass]
    public class AnagramDetectionTests
    {
        IAnagramDetectionService _service;
        [TestInitialize]
        public void Init()
        {
            _service = new AnagramDetectionService();
        }

        [TestMethod]
        public void AnagramDetection_Returns_Number_Of_Occurance()
        {
            int count = _service.DetectAnagram("AdnBndAndBdaBn", "dAn");
            Assert.AreEqual(4, count);
        }

        [TestMethod]
        public void AnagramDetection_Empty_Parent_String()
        {
            int count = _service.DetectAnagram("", "dAn");
            Assert.AreEqual(0, count);
        }

        [TestMethod]
        public void AnagramDetection_Empty_Query_String()
        {
            int count = _service.DetectAnagram("AdnBndAndBdaBn", "");
            Assert.AreEqual(0, count);
        }

        [TestMethod]
        public void AnagramDetection_Empty_Parent_Query_String()
        {
            int count = _service.DetectAnagram("", "");
            Assert.AreEqual(0, count);
        }
    }
}
